InspedioEngine
==============

J2ME Game Engine. Written by Wafi Harowa (a.k.a Hyude)

This Code is free to use for commercial purpose, as long as you keep the Inspedio logo and in-game mark.
If you wish you remove it completely, you can contact me to get permission.

Email me at harowa_aja@yahoo.co.id
